<?php

namespace App\Http\Controllers;

use App\Models\Character;
use App\Http\Requests\StoreCharacterRequest;
use App\Http\Requests\UpdateCharacterRequest;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCharacterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $split_input_1 = str_split(count_chars(str_replace(' ', '', strtolower($request->input_1)), 3), 1);
        $split_input_2 = str_split(count_chars(str_replace(' ', '', strtolower($request->input_2)), 3), 1);

        $total_char_input_1 = strlen($request->input_1);
        $total_same_char = 0;

        foreach($split_input_1 as $si1){
            foreach($split_input_2 as $si2){
                if($si1 == $si2){
                    $total_same_char += 1;
                }
            }
        }

        // dd($split_input_1, $split_input_2, $total_same_char, $total_char_input_1);

        $percentage = round($total_same_char / $total_char_input_1 * 100, 2);
        Character::create(
            [
                'input_1' => $request->input_1,
                'input_2' => $request->input_2,
                'total_char_input_1' => $total_char_input_1,
                'total_same_char' => $total_same_char,
                'percentage' => $percentage,
            ]
        );

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function show(Character $character)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function edit(Character $character)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCharacterRequest  $request
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCharacterRequest $request, Character $character)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function destroy(Character $character)
    {
        //
    }
}
