<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $character = Character::orderBy('id', 'desc')->first();

        return view('home', compact(
            'character'
        ));
    }
}
