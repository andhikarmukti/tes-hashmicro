@extends('layouts.main')

@section('content')
    <div class="flex flex-col gap-5 p-9">
        <div class="flex justify-center">
            <div class="border border-slate-300 rounded-lg shadow-md p-5">
                <div class="w-full h-full bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <div class="flex flex-col items-center p-10">
                        <img class="w-24 h-24 mb-3 rounded-full shadow-lg overflow-auto" src="/image/dika.jpg" alt="Foto Dika"/>
                        <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">Andhika Raharja Mukti</h5>
                        <span class="text-sm text-gray-500 dark:text-gray-400">Web Developer</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-col md:flex-row justify-center gap-5">
            <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full lg:w-1/2 xl:w-1/4">
                <form action="/character" method="POST">
                    @csrf
                    <div class="mb-6">
                      <label for="input_1" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Input 1</label>
                      <input type="text" id="input_1" name="input_1" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required autocomplete="off">
                    </div>
                    <div class="mb-6">
                        <label for="input_2" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Input 2</label>
                        <input type="text" id="input_2" name="input_2" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required autocomplete="off">
                      </div>
                    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
            <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full lg:w-1/2 xl:w-1/4">
                @if ($character)
                <div class="flex flex-col h-full">
                    <div class="flex flex-col gap-2 w-full">
                        <div class="border border-slate-200 shadow-sm rounded-lg w-full p-3 bg-blue-100">
                            Input 1 : <span class="font-bold">{{ $character->input_1 ?? '-' }}</span>
                        </div>
                        <div class="border border-slate-200 shadow-sm rounded-lg w-full p-3 bg-orange-100">
                            Input 2 : <span class="font-bold">{{ $character->input_2 ?? '-' }}</span>
                        </div>
                    </div>
                    <div class="flex justify-center items-center h-full mt-5 md:mt-0">
                        <h1 class="text-6xl font-bold">{{ $character->percentage ?? '-' }} %</h1>
                    </div>
                </div>
                @else
                <div class="flex flex-col justify-center items-center h-full">
                    <h1>Silahkan input terlebih dahulu</h1>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
